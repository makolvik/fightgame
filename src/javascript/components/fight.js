import { controls } from '../../constants/controls';
import { fighterService } from '../services/fightersService';

export async function fight(firstFighter, secondFighter) {
  const leftBar = document.getElementById('left-fighter-indicator');
  const rightBar = document.getElementById('right-fighter-indicator');
  let firstFighterHealth = 100;
  let secondFighterHealth = 100;
  let pressedKeys = [];
  const criticalAttack = { firstFighter: true, secondFighter: true };

  function setCriticalTimeout(fighter) {
    (criticalAttack[fighter] = false),
      setTimeout(function () {
        return (criticalAttack[fighter] = true);
      }, 10000);
  }

  return new Promise((resolve) => {
    window.addEventListener('keypress', (e) => {
      if (pressedKeys.indexOf(controls.PlayerOneBlock) < 0 && pressedKeys.indexOf(controls.PlayerTwoBlock) < 0) {
        if (e.code === controls.PlayerTwoAttack) {
          firstFighterHealth = updateStyleBar(firstFighterHealth, leftBar, 'normal', secondFighter, firstFighter);
        } else if (e.code === controls.PlayerOneAttack) {
          if (pressedKeys.indexOf(controls.PlayerTwoBlock) < 0) {
            secondFighterHealth = updateStyleBar(secondFighterHealth, rightBar, 'normal', firstFighter, secondFighter);
          }
        }
      }
      //if critical combination then attack
      //check if this combo
      if (checkComb(controls.PlayerTwoCriticalHitCombination, pressedKeys) && criticalAttack.secondFighter) {
        firstFighterHealth = updateStyleBar(firstFighterHealth, leftBar, 'critical', secondFighter, firstFighter);
        setCriticalTimeout('secondFighter');
      }
      if (checkComb(controls.PlayerOneCriticalHitCombination, pressedKeys) && criticalAttack.firstFighter) {
        secondFighterHealth = updateStyleBar(secondFighterHealth, rightBar, 'critical', firstFighter, secondFighter);
        setCriticalTimeout('firstFighter');
      }
      // resolve the promise with the winner when fight is over
      if (firstFighterHealth <= 0 || secondFighterHealth <= 0) {
        firstFighterHealth < 0 ? resolve(secondFighter) : resolve(firstFighter);
      }
    });
    window.addEventListener('keydown', (e) => {
      if (pressedKeys.indexOf(e.code) < 0) {
        pressedKeys.push(e.code);
      }
    });
    window.addEventListener('keyup', (e) => {
      if (pressedKeys.indexOf(e.code) >= 0) {
        pressedKeys.splice(pressedKeys.indexOf(e.code), 1);
      }
    });
  });
}

export function getDamage(attacker, defender) {
  // return damage
  let attack = 0;
  attack = getHitPower(attacker) - getBlockPower(defender);
  if (attack < 0) {
    return 0;
  } else {
    return attack;
  }
}
export function getCriticalDamage(attacker) {
  let attack = attacker.attack * 2;
  return attack;
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = Math.random() + 1;
  const attack = fighter.attack;
  const power = attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance = Math.random() + 1;
  const defense = fighter.defense;
  const power = defense * dodgeChance;
  return power;
}
export function checkComb(comb, pressed) {
  for (let key of comb) {
    if (pressed.indexOf(key) < 0) {
      return false;
    }
  }
  return true;
}

export function updateStyleBar(hp, bar, type, attacker, defenser) {
  if (type === 'critical') {
    console.log('cruur' + hp);
    hp = hp - getCriticalDamage(attacker);
    bar.style.width = hp.toString() + '%';
    return hp;
  } else {
    hp = hp - getDamage(attacker, defenser);
    bar.style.width = hp.toString() + '%';
    console.log(bar, hp);
    return hp;
  }
}
