import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const fighterName = fighter.name;
  const fighterHP = fighter.health;
  const fighterAttack = fighter.attack;
  const fighterDefense = fighter.defense;
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterSource = fighter.source;
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  // todo: show fighter info (image, name, health, etc.)
  const fightImageBlock = createElement({ tagName: 'div', className: 'preview-container_fightImageBlock' });

  const fighterNameElem = createElement({ tagName: 'h2', className: 'preview-infoBlock_header' });
  const fighterHealthElem = createElement({ tagName: 'h5', className: 'preview-infoBlock_header' });
  const fighterAttackElem = createElement({ tagName: 'h5', className: 'preview-infoBlock_header' });
  const fighterDefenseElem = createElement({ tagName: 'h5', className: 'preview-infoBlock_header' });

  const fighterImagePreview = createElement({
    tagName: 'img',
    className: 'fighter___fighter-image',
    attributes: {
      src: fighterSource,
      title: fighterName,
      alt: fighterName,
    },
  });

  fighterNameElem.innerHTML = `Name: ${fighterName}`;
  fighterHealthElem.innerHTML = `Health: ${fighterHP}`;
  fighterAttackElem.innerHTML = `Attack: ${fighterAttack}`;
  fighterDefenseElem.innerHTML = `Defense: ${fighterDefense}`;

  fightImageBlock.append(fighterImagePreview);
  fighterElement.append(fightImageBlock, fighterNameElem, fighterHealthElem, fighterAttackElem, fighterDefenseElem);

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
