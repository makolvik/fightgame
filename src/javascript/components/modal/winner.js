import { showModal } from './modal';
export function showWinnerModal(fighter) {
  // call showModal function
  const title = `GAME OVER`;
  const bodyElement = `${fighter.name}`;
  // const winner = fighter.name;
  showModal({ title, bodyElement });
}
